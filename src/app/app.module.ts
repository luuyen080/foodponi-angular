import {isDevMode, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {en_US, NZ_I18N} from 'ng-zorro-antd/i18n';
import {DecimalPipe, registerLocaleData} from '@angular/common';
import en from '@angular/common/locales/en';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoginPage} from "./page/login/login.page";
import {AppRoutingModule} from './app-routing.module';
import {DashboardPage} from "./page/dashboard/dashboard.page";
import {SignupPage} from "./page/signup/signup.page";
import {ForgotPasswordPage} from "./page/forgot-password/forgot-password.page";
import {MainAdminComponent} from "./layout/main-admin/main-admin.component";
import {SettingPage} from "./page/setting/setting.page";
import {NotificationSettingComponent} from "./component/notification-setting/notification-setting.component";
import {ProfileSettingComponent} from "./component/profile-setting/profile-setting.component";
import {AvatarButtonComponent} from "./component/avatar-button/avatar-button.component";
import {NotificationButtonComponent} from "./component/notification-button/notification-button.component";
import {ChangePasswordComponent} from "./component/change-password/change-password.component";
import {LanguageSelectorComponent} from "./component/language-selector/language-selector.component";
import {BreadcrumbComponent} from "./component/breadcrumb/breadcrumb.component";
import {AllProductPage} from "./page/product/list/all-product.page";
import {ProductCategoryPage} from "./page/product-category/product-category.page";
import {ProductFormPage} from "./page/product/form/product-form.page";
import {ProductDetailComponent} from "./page/product/form/product-detail/product-detail.component";
import {ProductInfoComponent} from "./page/product/form/product-info/product-info.component";
import {ProductThumbnailComponent} from "./page/product/form/product-thumbnail/product-thumbnail.component";
import {ProductCategoryComponent} from "./page/product/form/product-category/product-category.component";
import {AllUserPage} from "./page/user/list/all-user.page";
import {AntDesignModule} from "./ant-design.module";
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {NgxEditorModule} from "ngx-editor";
import {FileUploadPage} from "./page/file-upload/file-upload.page";
import {FileManagementComponent} from "./component/file-management/file-management.component";
import {InputPriceComponent} from "./component/input-price/input-price.component";
import {RichEditorComponent} from "./component/rich-editor/rich-editor.component";
import {AuthInterceptor} from "./_helper/auth.interceptor";
import {UserFeature} from "./store/user/user.reducer";
import {AllOrderPage} from "./page/order/list/all-order.page";
import {OrderDetailPage} from "./page/order/detail/order-detail.page";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzTimelineModule} from "ng-zorro-antd/timeline";
import {NzRadioModule} from "ng-zorro-antd/radio";
import {NzTagModule} from "ng-zorro-antd/tag";
import {NzResultModule} from "ng-zorro-antd/result";

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    MainAdminComponent,
    DashboardPage,
    AllUserPage,
    AllProductPage,
    ProductFormPage,
    ProductInfoComponent,
    ProductDetailComponent,
    ProductThumbnailComponent,
    ProductCategoryComponent,
    ProductCategoryPage,
    SettingPage,
    ProfileSettingComponent,
    NotificationSettingComponent,
    LoginPage,
    SignupPage,
    ForgotPasswordPage,
    AvatarButtonComponent,
    NotificationButtonComponent,
    ChangePasswordComponent,
    LanguageSelectorComponent,
    BreadcrumbComponent,
    FileUploadPage,
    FileManagementComponent,
    InputPriceComponent,
    RichEditorComponent,
    AllOrderPage,
    OrderDetailPage,
  ],
    imports: [
        AntDesignModule,
        NgxEditorModule,
        BrowserModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        ReactiveFormsModule,
        StoreModule.forRoot({}),
        StoreModule.forFeature(UserFeature),
        EffectsModule.forRoot(),
        StoreDevtoolsModule.instrument({maxAge: 25, logOnly: !isDevMode()}),
        NzPaginationModule,
        NzTimelineModule,
        NzRadioModule,
        NzTagModule,
        NzResultModule,
    ],
  providers: [
    {provide: NZ_I18N, useValue: en_US},
    DecimalPipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
