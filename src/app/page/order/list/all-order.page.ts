import {Component, OnInit} from "@angular/core";
import {OrderService} from "../../../service/order.service";
import {User} from "../../../model/user";
import {Address} from "../../../model/address";
import {UserSelectors} from "../../../store/user/user.reducer";
import {Store} from "@ngrx/store";

export interface IOrder {
  id: string;
  user: {
    avatar: string
  }
  shippingAddress: Address;
  status: boolean;
  totalAmount: number;
  createdDate: number;
}

@Component({
  selector: 'all-order-page',
  templateUrl: 'all-order.page.html',
  styleUrls: ['all-order.page.sass']
})
export class AllOrderPage implements OnInit {
  hasPermission: boolean;
  listOfData: IOrder[] = [];
  isLoading = false;
  total = 1;
  pageSize = 10;
  pageIndex = 1;

  constructor(
    private store: Store,
    private orderService: OrderService) {
  }

  ngOnInit(): void {
    this.store.select(UserSelectors.selectCurrentUser).subscribe(currentUser => {
      if (currentUser) {
        this.hasPermission = currentUser.role === 'RETAILER';
        if (this.hasPermission)
          this.loadDataFromServer(this.pageIndex, this.pageSize);
      }
    });
  }

  loadDataFromServer = (page: number, pageSize: number) => {
    this.isLoading = true;
    this.orderService.getOrders(page - 1, pageSize).subscribe(res => {
      this.isLoading = false;
      this.listOfData = res.content.map(order => {
        return {
          id: order.id,
          status: order.status,
          user: {
            avatar: order.user.avatar
          },
          shippingAddress: order.shippingAddress,
          totalAmount: order.totalAmount,
          createdDate: Math.floor((new Date().getTime() - new Date(order.createdDate).getTime()) / 60000),
        } as IOrder;
        this.total = res.totalElements;
      })
    });
  }

  protected readonly Math = Math;
}
