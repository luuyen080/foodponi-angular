import {Component, OnInit} from "@angular/core";
import {OrderService} from "../../../service/order.service";
import {ActivatedRoute} from "@angular/router";
import {User} from "../../../model/user";
import {INITIAL_ORDER, Order} from "../../../model/order";

@Component({
  selector: 'all-order-page',
  templateUrl: 'order-detail.page.html',
  styleUrls: ['order-detail.page.sass']
})
export class OrderDetailPage implements OnInit {
  order: Order = INITIAL_ORDER;
  isLoading = false;


  constructor(
    private route: ActivatedRoute,
    private orderService: OrderService) {
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.route.params.subscribe((params) => {
      this.orderService.getOrder(params['id']).subscribe(res => {
        this.isLoading = false;
        this.order = res;
      })

    })
  }


  protected readonly top = top;
}
