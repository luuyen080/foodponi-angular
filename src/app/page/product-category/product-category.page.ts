import {Component, OnInit} from '@angular/core';
import {ProductCategoryService} from "../../service/product-category.service";
import {UserSelectors} from "../../store/user/user.reducer";
import {IProduct} from "../product/list/all-product.page";
import {Store} from "@ngrx/store";

interface ICategoryProduct {
  id: string;
  categoryName: string;
  slug: string;
  description: string;
  thumbnail: string;
  isOnlyParent: () => boolean;
  categories: ICategoryProduct[]
}

@Component({
  selector: 'category-product-page',
  templateUrl: 'product-category.page.html',
  styleUrls: ['product-category.page.sass']
})
export class ProductCategoryPage implements OnInit {
  hasPermission: boolean;
  listOfData: ICategoryProduct[] = [];
  convertedCategories: ICategoryProduct[] = [];
  isLoading = false;
  total = 1;
  pageSize = 10;
  pageIndex = 1;

  constructor(
    private store: Store,
    private productCategoryService: ProductCategoryService) {
  }

  ngOnInit() {
    this.store.select(UserSelectors.selectCurrentUser).subscribe(currentUser => {
      if (currentUser) {
        this.hasPermission = currentUser.role === 'ADMIN';
        if (this.hasPermission)
          this.loadDataFromServer(this.pageIndex, this.pageSize);
      }
    });
  }

  loadDataFromServer(pageIndex: number, pageSize: number): void {
    this.isLoading = true;
    this.productCategoryService.getCategoriesIsOnlyParent().subscribe(categories => {
      this.listOfData = this.convertToICategoryProduct(categories.content);
      this.isLoading = false;
    });
  }

  convertToICategoryProduct(categories: any[], parentChar = ''): ICategoryProduct[] {

    categories.forEach((item: any) => {
      let char = parentChar;

      const convertedCategory: ICategoryProduct = {
        id: item.id,
        categoryName: char + item.categoryName,
        slug: item.slug,
        description: item.description,
        thumbnail: item.thumbnail,
        isOnlyParent: function () {
          return !this.categoryName.startsWith("—\t")
        },
        categories: item.categories
      };

      char = "—\t" + char;

      this.convertedCategories.push(convertedCategory);

      if (item.categories.length != 0) {
        this.convertToICategoryProduct(item.categories, char);
      }
    });

    return this.convertedCategories;
  }

}
