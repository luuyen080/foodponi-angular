import {Component, Injectable, OnInit, ViewChild} from '@angular/core';
import {NzFormatEmitEvent, NzTreeComponent, NzTreeNode} from "ng-zorro-antd/tree";
import {ProductCategoryService} from "../../../../service/product-category.service";
import {Category, CategoryRequest} from "../../../../model/category";


@Component({
  selector: 'product-category-component',
  templateUrl: 'product-category.component.html',
  styleUrls: ['product-category.component.sass']
})

@Injectable({
  providedIn: 'root'
})
export class ProductCategoryComponent implements OnInit {
  @ViewChild('treeComponent', {static: false}) treeComponent!: NzTreeComponent;
  nodes: any = [];
  isLoading = false;
  checkedKeys: CategoryRequest[] = [];

  constructor(
    private productCategoryService: ProductCategoryService) {
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.loadCategories([]);
  }

  public loadCategories(categoriesChecked: Category[]) {
    this.productCategoryService.getCategoriesIsOnlyParent().subscribe((categories) => {
      this.isLoading = false;
      this.nodes = categories.content.map((category: Category): any => {
        let node = this.mapToNote(category);
        node["isOnlyParent"] = true;
        if (category.default) {
          this.checkedKeys.push({id: category.id});
        }
        return node;
      });
    });
  }

  private mapToNote(category: Category) {
    let node: any = {};
    for (const key in category) {
      if (category.hasOwnProperty(key)) {
        if (key === "id") node["key"] = category[key];
        if (key === "categoryName") node["title"] = category[key];
        if (key === "categories")
          if (category[key].length) {
            node["isLeaf"] = false;
            node["expanded"] = true;
            node["children"] = category[key]
              .map((category: Category) => this.mapToNote(category));
          } else node["isLeaf"] = true;
      }
    }

    return node;
  }

  keyDisable?: NzTreeNode;

  nzEvent(event: NzFormatEmitEvent): void {
    this.checkedKeys = [];
    if (event.checkedKeys) {
      event.checkedKeys.forEach(key => {
        if (key.isDisabled) {
          key.setChecked(false);
          this.keyDisable = event.checkedKeys?.at(0);
          event.checkedKeys?.splice(0, 1);
        }
      })
    }
    if (event.checkedKeys?.length == 0) {
      this.keyDisable?.setChecked(true);
      this.checkedKeys.push({id: this.keyDisable!.key})
    }
    event.checkedKeys?.forEach(node => this.childrenNodeToCategory(node));
    event.checkedKeys?.forEach(node => this.parentNodeToCategory(node));
  }

  private childrenNodeToCategory(node: NzTreeNode) {
    let category: CategoryRequest = {id: node.key};
    node.children.map((child) => this.childrenNodeToCategory(child));
    this.checkedKeys?.push(category);
  }

  private parentNodeToCategory(node: NzTreeNode) {
    let category: CategoryRequest = {id: node.key};
    if (node.parentNode)
      this.parentNodeToCategory(node.parentNode);
    if (!this.checkedKeys.some(c => c.id === category.id))
      this.checkedKeys?.push(category);
  }
}
