import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {
  AbstractControl,
  AsyncValidatorFn,
  FormControl,
  FormGroup,
  NonNullableFormBuilder, ValidationErrors,
  Validators
} from "@angular/forms";
import {Store} from "@ngrx/store";
import {Editor, Toolbar} from "ngx-editor";
import {Observable, Observer} from "rxjs";
import {ProductService} from "../../../../service/product.service";

@Component({
  selector: 'product-info-component',
  templateUrl: 'product-info.component.html',
  styleUrls: ['product-info.component.sass']
})

export class ProductInfoComponent implements OnInit {
  @Input() isUpdate: any;
  isLoading: boolean = true;
  productForm: FormGroup<{
    id: FormControl<string>
    name: FormControl<string>;
    slug: FormControl<string>;
    shortDescription: FormControl<string>;
  }>;
  editor: Editor;

  constructor(
    private productService: ProductService,
    private store: Store,
    private fb: NonNullableFormBuilder,
    private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.editor = new Editor();
    this.productForm = this.fb.group({
      id: [''],
      name: ['', [Validators.required]],
      slug: ['', [Validators.required], [this.slugAsyncValidator]],
      shortDescription: ['']
    });
  }

  timeout = setTimeout(() => {});
  slugAsyncValidator: AsyncValidatorFn = (control: AbstractControl) =>
    new Observable((observer: Observer<ValidationErrors | null>) => {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        this.productService.isExistedBySlug(control.value).subscribe((isExisted) => {
          if (isExisted) {
            // you have to return `{error: true}` to mark it as an error event
            observer.next({error: true, duplicated: true});
          } else {
            observer.next(null);
          }
          observer.complete();
        })
      }, 1000);
    });
  protected readonly window = window;
}
