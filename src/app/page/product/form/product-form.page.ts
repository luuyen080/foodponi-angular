import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {NonNullableFormBuilder} from "@angular/forms";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {ProductInfoComponent} from "./product-info/product-info.component";
import {ProductDetailComponent} from "./product-detail/product-detail.component";
import {ProductCategoryComponent} from "./product-category/product-category.component";
import {ProductService} from "../../../service/product.service";
import {ProductThumbnailComponent} from "./product-thumbnail/product-thumbnail.component";
import {Store} from "@ngrx/store";
import {ProductCategoryService} from "../../../service/product-category.service";
import {ProductRequest} from "../../../model/product";
import {ProductDetailRequest} from "../../../model/product";
import {AuthService} from "../../../service/auth.service";
import {UserSelectors} from "../../../store/user/user.reducer";

@Component({
  selector: 'product-form-page',
  templateUrl: 'product-form.page.html',
  styleUrls: ['product-form.page.sass']
})
export class ProductFormPage implements OnInit {
  @ViewChild(ProductInfoComponent) productInfoRef!: ProductInfoComponent;
  @ViewChild(ProductDetailComponent) productDetailRef!: ProductDetailComponent;
  @ViewChild(ProductCategoryComponent) productCategoryRef!: ProductCategoryComponent;
  @ViewChild(ProductThumbnailComponent) productThumbnailRef!: ProductThumbnailComponent;

  hasPermission: boolean;
  pageState = {title: 'Add New Product', isUpdate: false};
  isPending = false;

  constructor(
    private authService: AuthService,
    private productCategoryService: ProductCategoryService,
    private store: Store,
    private productService: ProductService,
    private fb: NonNullableFormBuilder,
    private route: ActivatedRoute,
    private notification: NzNotificationService,
    private router: Router) {
  }

  ngOnInit() {
    this.store.select(UserSelectors.selectCurrentUser).subscribe(currentUser => {
      if (currentUser) {
        this.hasPermission = currentUser.role === 'RETAILER';
        if (this.hasPermission)
          this.route.params.subscribe(params => {
              if (params['id']) {
                this.pageState = {title: "Update Product", isUpdate: true};
                this.productService.getProduct(params['id']).subscribe({
                  next: res => {
                    this.productInfoRef.isLoading = false;
                    // set product info
                    this.productInfoRef.productForm.patchValue({
                      id: res.id,
                      name: res.name,
                      slug: res.slug,
                      shortDescription: res.shortDescription
                    });

                    // set product thumbnail
                    this.productThumbnailRef.imageUrl = res.thumbnail;

                    // set product categories
                    this.productCategoryRef.loadCategories(res.categories);

                    // set product detail
                    this.productDetailRef.isLoading = false;
                    this.productDetailRef.segmentedState.index = res.productDetails.length > 1 ? 1 : 0;
                    res.productDetails.forEach((productDetail, index) => {
                      this.productDetailRef.formDetails.controls[index].patchValue({
                        id: productDetail.id,
                        productDetailName: productDetail.name,
                        productPrice: productDetail.price,
                        description: productDetail.description
                      });
                      this.productDetailRef.images[index] = productDetail.images;
                    });
                  }
                });
              }
            }
          );
      }
    });
  }

  save(status: boolean): void {
    const checkDetail = this.productDetailRef.formDetails.controls.some(formGroup => !formGroup.valid);
    if (this.productInfoRef.productForm.valid) {
      if (!this.productDetailRef.formDetails.controls.some(formGroup => !formGroup.valid)) {
        const productRequest: ProductRequest = {
          id: this.productInfoRef.productForm.value.id ? this.productInfoRef.productForm.value.id : undefined,
          name: this.productInfoRef.productForm.value.name!,
          slug: this.productInfoRef.productForm.value.slug!,
          shortDescription: this.productInfoRef.productForm.value.shortDescription,
          thumbnail: this.productThumbnailRef.imageUrl,
          user: {id: this.authService.getIdCurrentUser()},
          categories: this.productCategoryRef.checkedKeys,
          status: status,
          productDetails: this.productDetailRef.formDetails.controls.map((formGroup, index) => {
            return {
              id: formGroup.value.id ? formGroup.value.id : undefined,
              name: formGroup.value.productDetailName,
              price: formGroup.controls.productPrice.value.replaceAll(',', ''),
              description: formGroup.value.description,
              images: this.productDetailRef.images[index]
            } as ProductDetailRequest
          })
        }

        const observable = this.pageState.isUpdate ?
          this.productService.updateProduct(productRequest.id!, productRequest) :
          this.productService.createProduct(productRequest)

        this.isPending = true;
        observable.subscribe({
          complete: () => {
            this.isPending = false;
            this.notification.success(
              'Success',
              'Product saved successfully!'
            );
            this.router.navigate(['/products']);
          },
          error: () => {
            this.isPending = false;
            this.notification.error(
              'Error',
              'Failed to save product!'
            );
          }
        })

      } else {
        this.productDetailRef.formDetails.controls.forEach((formGroup, index) => {
          Object.values(formGroup.controls).forEach(control => {
            if (control.invalid) {
              this.productDetailRef.tabSelectedIndex = index;
              control.markAsDirty();
              control.updateValueAndValidity({onlySelf: true});
            }
          });
        });
      }
    } else {
      Object.values(this.productInfoRef.productForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({onlySelf: true});
        }
      });
    }
  }
}
