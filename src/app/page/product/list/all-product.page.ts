import {Component, OnInit} from '@angular/core';
import {NzTableQueryParams} from "ng-zorro-antd/table";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {NzModalService} from "ng-zorro-antd/modal";
import {ProductService} from "../../../service/product.service";
import {Store} from "@ngrx/store";
import {UserSelectors} from "../../../store/user/user.reducer";

export interface IProduct {
  id: string;
  name: string;
  thumbnail: string | null;
  status: boolean;
  minPrice: number;
  maxPrice: number;
  categories: string;
}

@Component({
  selector: 'all-product-page',
  templateUrl: 'all-product.page.html',
  styleUrls: ['all-product.page.sass']
})
export class AllProductPage implements OnInit {
  hasPermission: boolean;
  sortFnName = (a: IProduct, b: IProduct) => a.name.localeCompare(b.name);
  sortFnPrice = (a: IProduct, b: IProduct) => a.maxPrice - b.minPrice;
  filterFnStatus = (list: string[], item: IProduct) => list.includes(String(item.status));

  filterStatus = [
    {text: 'true', value: 'true'},
    {text: 'false', value: 'false'}
  ];

  listOfData: IProduct[] = [];
  isLoading = false;
  total = 1;
  pageSize = 10;
  pageIndex = 1;

  constructor(
    private store: Store,
    private productService: ProductService,
    private notification: NzNotificationService,
    private modal: NzModalService) {
  }

  ngOnInit(): void {
    this.store.select(UserSelectors.selectCurrentUser).subscribe(currentUser => {
      if (currentUser) {
        this.hasPermission = currentUser.role === 'RETAILER';
        if (this.hasPermission)
          this.loadDataFromServer(this.pageIndex, this.pageSize);
      }
    });
  }

  loadDataFromServer(pageIndex: number, pageSize: number): void {
    this.isLoading = true;
    this.productService.getProducts(pageIndex - 1, pageSize).subscribe({
      next: res => {
        this.listOfData = (res.content).map((product): IProduct => {
          return {
            id: product.id,
            name: product.name,
            thumbnail: product.thumbnail,
            status: product.status,
            minPrice: Math.min(...product.productDetails.map(productDetail => Number(productDetail.price))),
            maxPrice: Math.max(...product.productDetails.map(productDetail => Number(productDetail.price))),
            categories: product.categories.map(category => category.categoryName).join(', ')
          }
        });
        this.total = res.totalElements;
        this.isLoading = false;
      }
    })
  }

  queryParamsChangeEventCnt = 0;

  onQueryParamsChange(params: NzTableQueryParams): void {
    if (++this.queryParamsChangeEventCnt == 1) return;
    const {pageSize, pageIndex} = params;
    this.pageIndex = pageIndex;
    this.loadDataFromServer(pageIndex, pageSize);
  }

  delete(id: string): void {
    this.modal.confirm({
      nzTitle: 'Delete',
      nzOkText: 'Yes',
      nzCancelText: 'No',
      nzOkType: 'primary',
      nzOkDanger: true,
      nzContent: 'Do you want to delete this product?',
      nzClosable: false,
      nzOnOk: () => new Promise(resolve => {
        this.store.select(UserSelectors.selectCurrentUser).subscribe((currentUsers) => {
          this.productService.deleteProduct(id).subscribe({
            complete: () => {
              this.loadDataFromServer(this.pageIndex, this.pageSize);
              resolve();
            },
            error: () => resolve()
          });
        });
      })
    });
  }
}
