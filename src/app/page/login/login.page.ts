import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, NonNullableFormBuilder, Validators} from "@angular/forms";
import {AuthService} from "../../service/auth.service";
import {CurrentUser, IUserRemember, UserRequestLogin} from "../../model/user";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {CookieService} from "ngx-cookie-service";
import {Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {INITIAL_CURRENT_USER, UserActions} from "../../store/user/user.reducer";
import jwtDecode from "jwt-decode";
import {IToken} from "../../model/auth";
import {REFRESH_TOKEN, REMEMBER_ME} from "../../utils/server";

@Component({
  selector: 'login-page',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.sass']
})
export class LoginPage implements OnInit {
  validateForm: FormGroup<{
    userName: FormControl<string>;
    password: FormControl<string>;
    remember: FormControl<boolean>;
  }> = this.fb.group({
    userName: ['', [Validators.required]],
    password: ['', [Validators.required]],
    remember: [true]
  });

  isPending: boolean = false;
  currentUser: CurrentUser = INITIAL_CURRENT_USER;

  constructor(
    private store: Store,
    private fb: NonNullableFormBuilder,
    private authService: AuthService,
    private notify: NzNotificationService,
    private cookieService: CookieService,
    private router: Router) {
  }

  ngOnInit(): void {
    const rememberMe = this.cookieService.get("rememberMe");
    if (rememberMe) {
      const user = JSON.parse(atob(rememberMe)) as IUserRemember;
      this.validateForm.patchValue({
        userName: user.username,
        password: user.password,
        remember: true
      });
    }
    this.validateForm.get("userName")?.valueChanges.subscribe((value) => {
      if (value.includes("@")) {
        this.validateForm.controls.userName.addValidators(Validators.email);
        this.validateForm.controls.userName.updateValueAndValidity({emitEvent: false});
      } else {
        this.validateForm.get("userName")?.removeValidators(Validators.email);
        this.validateForm.controls.userName.updateValueAndValidity({emitEvent: false});
      }
    })
  }

  submitForm(): void {
    if (this.validateForm.valid) {
      let {userName = '', password = ''} = this.validateForm.value;

      // set user login
      let user: UserRequestLogin = this.validateForm.controls.userName.value.includes("@")
        ? {username: null, email: userName, password: password}
        : {username: userName, email: null, password: password}

      // login action
      this.isPending = true;
      this.authService.login(user).subscribe({
        next: res => {
          this.isPending = false;

          const {accessToken, refreshToken} = res as IToken;
          this.currentUser = jwtDecode(accessToken) as CurrentUser;

          // set refresh token
          this.cookieService.set(REFRESH_TOKEN, refreshToken, {
            expires: new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30),
          });

          //set user remembered and delete
          if (this.validateForm.controls.remember.value) {
            const userRemember: IUserRemember = {
              username: user.username ? user.username! : user.email!,
              password: user.password,
              avatar: this.currentUser.avatar
            }
            this.cookieService.set(REMEMBER_ME, btoa(JSON.stringify(userRemember)), {
              expires: new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30),
            });
          } else this.cookieService.delete(REMEMBER_ME);

          // redirect
          if (this.currentUser.role === 'CUSTOMER') {
            this.cookieService.delete(REFRESH_TOKEN);
            this.cookieService.delete(REMEMBER_ME);
            this.notify.warning("Login message", "You don't have permission");
          } else {
            this.notify.success("Login message", "Login successfully");
            this.router.navigate(["/"]);
          }
        },
        error: res => {
          this.isPending = false;
          this.notify.create('error', 'Login Message', res.error ? res.error.error.message : 'Server Error');
        }
      })

    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({onlySelf: true});
        }
      });
    }
  }

}
