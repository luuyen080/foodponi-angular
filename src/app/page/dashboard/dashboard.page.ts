import {Component} from '@angular/core';

@Component({
  selector: 'dashboard-page',
  templateUrl: 'dashboard.page.html',
  styleUrls: ['dashboard.page.sass']
})
export class DashboardPage {
  isVertical = false;
}
