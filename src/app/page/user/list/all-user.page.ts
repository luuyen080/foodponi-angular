import {Component, OnInit} from "@angular/core";
import {NzTableQueryParams} from "ng-zorro-antd/table";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {NzModalService} from "ng-zorro-antd/modal";
import {UserService} from "../../../service/user.service";
import {User} from "../../../model/user";
import {Store} from "@ngrx/store";
import {UserSelectors} from "../../../store/user/user.reducer";

export interface IUser {
  id: string;
  avatar: string;
  email: string;
  fullName: string;
  phoneNumber: string;
  username: string;
  role: string;
  status: boolean;
}

@Component({
  selector: 'all-user-page',
  templateUrl: 'all-user.page.html',
  styleUrls: ['all-user.page.sass']
})
export class AllUserPage implements OnInit {
  hasPermission: boolean;
  sortFnEmail = (a: IUser, b: IUser) => a.email.localeCompare(b.email);
  sortFnFullName = (a: IUser, b: IUser) => a.fullName.localeCompare(b.fullName);
  sortFnPhoneNumber = (a: IUser, b: IUser) => a.phoneNumber.localeCompare(b.phoneNumber);
  sortFnUsername = (a: IUser, b: IUser) => a.username.localeCompare(b.username);
  filterFnRole = (list: string[], item: IUser) => list.some(name => item.role.indexOf(name) !== -1)
  filterFnStatus = (list: string[], item: IUser) => list.includes(String(item.status));

  filterRole = [
    {text: 'CUSTOMER', value: 'CUSTOMER'},
    {text: 'RETAILER', value: 'RETAILER'},
    {text: 'ADMIN', value: 'ADMIN'}
  ];
  filterStatus = [
    {text: 'true', value: 'true'},
    {text: 'false', value: 'false'}
  ];

  total = 1;
  listOfData: IUser[] = [];
  isLoading = false;
  pageSize = 10;
  pageIndex = 1;

  constructor(
    private store: Store,
    private userService: UserService,
    private notification: NzNotificationService,
    private modal: NzModalService) {
  }

  ngOnInit(): void {
    this.store.select(UserSelectors.selectCurrentUser).subscribe(currentUser => {
      if (currentUser) {
        this.hasPermission = currentUser.role === 'ADMIN';
        if (this.hasPermission)
          this.loadDataFromServer(this.pageIndex, this.pageSize);
      }
    });
  }

  loadDataFromServer(pageIndex: number, pageSize: number): void {
    this.isLoading = true;
    this.userService.getUsers(pageIndex - 1, pageSize).subscribe({
      next: res => {
        this.isLoading = false;
        this.listOfData = (res.content).map(user => {
          return {
            id: user.id,
            avatar: user.avatar,
            email: user.email,
            fullName: user.address?.fullName,
            phoneNumber: user.address?.phoneNumber,
            username: user.username,
            role: user.role,
            status: user.status
          } as IUser
        });
        this.total = res.totalElements;
      },
      error: res => {
        this.isLoading = false;
      }
    });
  }

  queryParamsChangeEventCnt = 0;

  onQueryParamsChange(params: NzTableQueryParams): void {
    if (++this.queryParamsChangeEventCnt == 1) return;
    const {pageSize, pageIndex} = params;
    this.pageIndex = pageIndex;
    this.loadDataFromServer(pageIndex, pageSize);
  }

  delete(id: string): void {
    this.modal.confirm({
      nzTitle: 'Delete',
      nzOkText: 'Yes',
      nzCancelText: 'No',
      nzOkType: 'primary',
      nzOkDanger: true,
      nzContent: 'Do you want to delete this user?',
      nzClosable: false,
      nzOnOk: () => new Promise(resolve => {
        this.userService.deleteUser(id).subscribe({
          complete: () => {
            this.loadDataFromServer(this.pageIndex, this.pageSize);
            resolve();
          },
          error: () => resolve()
        })
      })
    });
  }

}
