import {Component, OnInit} from '@angular/core';
import {CookieService} from "ngx-cookie-service";
import {Router} from "@angular/router";
import {Store} from "@ngrx/store";
import jwtDecode from "jwt-decode";
import {CurrentUser} from "../../model/user";
import {UserActions} from "../../store/user/user.reducer";
import {AuthService} from "../../service/auth.service";
import {IToken} from "../../model/auth";
import {REFRESH_TOKEN} from "../../utils/server";


@Component({
  selector: 'main-admin-layout',
  templateUrl: 'main-admin.component.html',
  styleUrls: ['main-admin.component.sass']
})
export class MainAdminComponent implements OnInit {
  title: string;
  isCollapsed = false;

  constructor(
    private authService: AuthService,
    private store: Store,
    private router: Router,
    private cookieService: CookieService) {
  }

  ngOnInit(): void {
    const refreshToken = this.cookieService.get(REFRESH_TOKEN);
    if (refreshToken) {
      this.authService.refreshToken(refreshToken).subscribe({
        next: (res) => {
          const {accessToken, refreshToken} = res as IToken;

          const currentUser: CurrentUser = {...jwtDecode(accessToken) as CurrentUser, accessToken};
          this.title = currentUser.role;
          if (currentUser.role === 'CUSTOMER' || currentUser.role === '') {
            this.cookieService.delete(REFRESH_TOKEN);
            this.router.navigate(['/login']);
          }
          this.store.dispatch(UserActions.setCurrentUser(currentUser));
          this.cookieService.set(REFRESH_TOKEN, refreshToken, {
            expires: new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30),
          });
        },
        error: (err) => {
          this.cookieService.delete(REFRESH_TOKEN);
          this.router.navigate(['/login']);
        }
      });
    } else this.router.navigate(['/login']);

  }

  toggleCollapsed(): void {
    this.isCollapsed = !this.isCollapsed;
  }

}
