import {Component} from '@angular/core';

@Component({
  selector: 'language-selector',
  templateUrl: 'language-selector.component.html',
  styleUrls: ['language-selector.component.sass']
})
export class LanguageSelectorComponent {
  selectedValue = "English";
}
