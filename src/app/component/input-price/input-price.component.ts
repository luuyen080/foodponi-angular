import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {DecimalPipe} from "@angular/common";

@Component({
  selector: 'input-price-component',
  templateUrl: 'input-price.component.html',
  styleUrls: ['input-price.component.sass']
})
export class InputPriceComponent implements OnInit {
  @Input() formGroup: FormGroup;

  constructor(private decimalPipe: DecimalPipe) {
  }

  ngOnInit(): void {
    this.formGroup.get("productPrice")?.valueChanges.subscribe((value) => {
      const formattedValue = this.getFormattedValue(value);
      this.formGroup.get("productPrice")?.patchValue(formattedValue, {emitEvent: false});
    });
  }

  getFormattedValue(value: any): string {
    const stringToTransform = String(value ?? '')
      .replace(/\D/g, '')
      .replace(/^0+/, '');
    return (
      '' +
      this.decimalPipe.transform(
        stringToTransform === '' ? '0' : stringToTransform,
        '1.0'
      )
    );
  }
}
