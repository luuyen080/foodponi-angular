import {Component, OnInit} from '@angular/core';
import {CookieService} from "ngx-cookie-service";
import {Router} from "@angular/router";
import {NzModalService} from "ng-zorro-antd/modal";
import {Store} from "@ngrx/store";
import {CurrentUser, IUserRemember} from "../../model/user";
import {INITIAL_CURRENT_USER, UserActions, UserSelectors} from "../../store/user/user.reducer";
import {REFRESH_TOKEN} from "../../utils/server";

declare var atob: any
declare var btoa: any

@Component({
  selector: 'avatar-button',
  templateUrl: 'avatar-button.component.html',
  styleUrls: ['avatar-button.component.sass']
})
export class AvatarButtonComponent implements OnInit {

  currentUser: CurrentUser = INITIAL_CURRENT_USER;

  constructor(
    private store: Store,
    private modal: NzModalService,
    private router: Router,
    private cookieService: CookieService) {
  }

  ngOnInit(): void {
    this.store.select(UserSelectors.selectCurrentUser).subscribe((currentUser) => {
      if (currentUser) this.currentUser = currentUser;
    });
  }

  logout = () => {
    this.modal.confirm({
      nzTitle: 'Logout',
      nzContent: 'Are you sure you want to logout?',
      nzOkText: 'Yes',
      nzCancelText: 'No',
      nzOkDisabled: false,
      nzOkDanger: true,
      nzOnOk: () => {
        // delete refresh token
        this.cookieService.delete(REFRESH_TOKEN);

        // delete store
        this.store.dispatch(UserActions.setCurrentUser(INITIAL_CURRENT_USER));

        //redirect to login page
        this.router.navigate(['/login']);
      }
    })
  }
}
