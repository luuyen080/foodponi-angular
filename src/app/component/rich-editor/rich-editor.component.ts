import {Component, Input} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {Editor, Toolbar} from "ngx-editor";

@Component({
  selector: 'rich-editor-component',
  templateUrl: 'rich-editor.component.html',
  styleUrls: ['rich-editor.component.sass']
})
export class RichEditorComponent {
  @Input() formGroup: FormGroup;
  @Input() controlName: string;
  @Input() title: string;
  @Input() editor: Editor;

  toolbars: Toolbar = [
    ['bold', 'italic'],
    ['underline', 'strike'],
    ['code', 'blockquote'],
    ['ordered_list', 'bullet_list'],
    [{heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']}],
    ['link', 'image'],
    ['text_color', 'background_color'],
    ['align_left', 'align_center', 'align_right', 'align_justify'],
    ['horizontal_rule', 'format_clear'],
  ];
}
