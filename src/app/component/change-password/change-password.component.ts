import {Component} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, NonNullableFormBuilder, ValidatorFn, Validators} from "@angular/forms";
import {UserService} from "../../service/user.service";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {NzModalRef, NzModalService} from "ng-zorro-antd/modal";

@Component({
  selector: 'change-password',
  templateUrl: 'change-password.component.html',
  styleUrls: ['change-password.component.sass']
})
export class ChangePasswordComponent {
  validateForm: FormGroup<{
    oldPassword: FormControl<string>;
    newPassword: FormControl<string>;
    confirmPassword: FormControl<string>;
  }>;

  confirmModal?: NzModalRef;
  isLoading = false;

  submitForm(resolve: () => void): void {
    let {oldPassword, newPassword} = this.validateForm.value;
    if (oldPassword && newPassword) {
      this.isLoading = true;
      this.userService.changePassword(oldPassword, newPassword).subscribe({
        error: res => {
          this.isLoading = false;
          resolve();

          const errorDetails = res.error.error.details;
          let errorMessages = [res.error.error.message];

          if (errorDetails && errorDetails.currentPassword) {
            errorMessages.push("Curent password: " + errorDetails.currentPassword);
          }

          if (errorDetails && errorDetails.newPassword) {
            errorMessages.push("New password: " + errorDetails.newPassword);
          }

          for (let errorMessage of errorMessages) {
            this.notification.create(
              "error",
              'Change password',
              errorMessage
            );
          }
        },
        complete: () => {
          this.isLoading = false;
          this.notification.create(
            "success",
            'Change password',
            'Change password successfully'
          );
          this.validateForm.reset();
          resolve();
        }
      });
    }
  }

  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() => this.validateForm.controls.confirmPassword.updateValueAndValidity());
  }

  confirmationValidator: ValidatorFn = (control: AbstractControl): { [s: string]: boolean } => {
    if (!control.value) {
      return {required: true};
    } else if (control.value !== this.validateForm.controls.newPassword.value) {
      return {confirm: true, error: true};
    }
    return {};
  };

  showConfirm(): void {
    if (this.validateForm.valid) {
      this.modal.confirm({
        nzTitle: 'Change password',
        nzContent: 'Do you want to change your password?',
        nzClosable: false,
        nzOnOk: () => new Promise(resolve => {
          this.submitForm(resolve);
        })
      });
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({onlySelf: true});
        }
      });
    }
  }

  constructor(private fb: NonNullableFormBuilder, private userService: UserService, private notification: NzNotificationService, private modal: NzModalService) {
    this.validateForm = this.fb.group({
      oldPassword: ['', [Validators.required]],
      newPassword: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required, this.confirmationValidator]]
    });
  }
}
