import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../../service/user.service';
import {Store} from '@ngrx/store';
import {UserActions, UserSelectors} from '../../store/user/user.reducer';
import {FileManagementComponent} from '../file-management/file-management.component';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormControl,
  FormGroup,
  NonNullableFormBuilder,
  ValidationErrors,
  Validators
} from '@angular/forms';
import {Observable, Observer} from 'rxjs';

@Component({
  selector: 'profile-setting',
  templateUrl: 'profile-setting.component.html',
  styleUrls: ['profile-setting.component.sass']
})
export class ProfileSettingComponent implements OnInit {
  @ViewChild(FileManagementComponent) fileManagementRef!: FileManagementComponent;

  isPending: boolean = false;
  imageUrl: string = '';
  isModalVisible: boolean = false;
  userForm: FormGroup<{
    username: FormControl<string>;
  }>;

  constructor(
    private store: Store,
    private userService: UserService,
    private fb: NonNullableFormBuilder) {
  }

  ngOnInit(): void {
    this.store.select(UserSelectors.selectCurrentUser).subscribe((currentUser) => {
      if (currentUser) {
        this.imageUrl = currentUser.avatar;
        this.userForm = this.fb.group({
          username: [currentUser.username, [Validators.required]]
        });
      }
    })
  }

  timeout = setTimeout(() => {
  });
  usernameAsyncValidator: AsyncValidatorFn = (control: AbstractControl) =>
    new Observable((observer: Observer<ValidationErrors | null>) => {
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        // this.userService.updateAvatar({username: control.value, avatar: this.imageUrl}).subscribe((isExisted) => {
        //   if (isExisted) {
        //     // you have to return `{error: true}` to mark it as an error event
        //     observer.next({error: true, duplicated: true});
        //   } else {
        //     observer.next(null);
        //   }
        //   observer.complete();
        // })
      }, 1000);
    });

  updateAvatar = () => {
    this.isModalVisible = !this.isModalVisible;
    this.isPending = true;
    this.imageUrl = this.fileManagementRef.selectedFile.url;
    if (this.imageUrl)
      this.userService.updateAvatar({avatar: this.imageUrl}).subscribe({
        next: () => {
          this.store.dispatch(UserActions.updateAvatar({avatar: this.imageUrl}));
          this.isPending = false;
        },
        error: () => {
          this.imageUrl = '';
          this.isPending = false;
        }
      })
  }


  protected readonly window = window;
}
