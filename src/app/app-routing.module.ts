import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {LoginPage} from "./page/login/login.page";
import {DashboardPage} from "./page/dashboard/dashboard.page";
import {SignupPage} from "./page/signup/signup.page";
import {ForgotPasswordPage} from "./page/forgot-password/forgot-password.page";
import {MainAdminComponent} from "./layout/main-admin/main-admin.component";
import {SettingPage} from "./page/setting/setting.page";
import {AllProductPage} from "./page/product/list/all-product.page";
import {ProductCategoryPage} from "./page/product-category/product-category.page";
import {ProductFormPage} from "./page/product/form/product-form.page";
import {AllUserPage} from "./page/user/list/all-user.page";
import {FileUploadPage} from "./page/file-upload/file-upload.page";
import {AllOrderPage} from "./page/order/list/all-order.page";
import {OrderDetailPage} from "./page/order/detail/order-detail.page";


const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {
    path: '',
    component: MainAdminComponent,
    children: [
      {path: 'dashboard', component: DashboardPage},
      {path: 'setting', component: SettingPage},
      {path: 'products', component: AllProductPage},
      {path: 'add-product', component: ProductFormPage},
      {path: 'products/:id', component: ProductFormPage},
      {path: 'category-product', component: ProductCategoryPage},
      {path: 'users', component: AllUserPage},
      {path: 'files', component: FileUploadPage},
      {path: 'orders', component: AllOrderPage},
      {path: 'orders/:id', component: OrderDetailPage},
    ]
  },
  {path: 'login', component: LoginPage},
  {path: 'signup', component: SignupPage},
  {path: 'forgot-password', component: ForgotPasswordPage},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
