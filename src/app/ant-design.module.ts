import {NgModule} from "@angular/core";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzAvatarModule} from "ng-zorro-antd/avatar";
import {NzBreadCrumbModule} from "ng-zorro-antd/breadcrumb";
import {NzMenuModule} from "ng-zorro-antd/menu";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzTypographyModule} from "ng-zorro-antd/typography";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {NzBadgeModule} from "ng-zorro-antd/badge";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzSkeletonModule} from "ng-zorro-antd/skeleton";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzModalModule} from "ng-zorro-antd/modal";
import {NzTabsModule} from "ng-zorro-antd/tabs";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzSwitchModule} from "ng-zorro-antd/switch";
import {NzImageModule} from "ng-zorro-antd/image";
import {NzTreeModule} from "ng-zorro-antd/tree";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzTimePickerModule} from "ng-zorro-antd/time-picker";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";
import {NzInputNumberModule} from "ng-zorro-antd/input-number";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzTreeViewModule} from "ng-zorro-antd/tree-view";
import {NzListModule} from "ng-zorro-antd/list";
import {NzSegmentedModule} from "ng-zorro-antd/segmented";
import {NzDescriptionsModule} from "ng-zorro-antd/descriptions";
import {NzPageHeaderModule} from "ng-zorro-antd/page-header";

@NgModule({
  exports: [
    NzLayoutModule,
    NzCheckboxModule,
    NzAvatarModule,
    NzBreadCrumbModule,
    NzMenuModule,
    NzIconModule,
    NzToolTipModule,
    NzTypographyModule,
    NzDividerModule,
    NzBadgeModule,
    NzDropDownModule,
    NzCardModule,
    NzSkeletonModule,
    NzTableModule,
    NzModalModule,
    NzTabsModule,
    NzUploadModule,
    NzSwitchModule,
    NzImageModule,
    NzTreeModule,
    NzButtonModule,
    NzFormModule,
    NzTimePickerModule,
    NzDatePickerModule,
    NzInputNumberModule,
    NzInputModule,
    NzSelectModule,
    NzTreeViewModule,
    NzListModule,
    NzSegmentedModule,
    NzDescriptionsModule,
    NzPageHeaderModule
  ]})
export class AntDesignModule {}
