export interface FileUpload {

  id: string,

  name: string,

  extension: string,

  contentType: string,

  size: number,

  url: string,

  dimension: {
    width: number,
    height: number
  }

}

export const INITIAL_FILE_UPLOAD = {
  id: '',
  name: '',
  extension: '',
  contentType: '',
  size: -1,
  url: '',
  dimension: {
    width: 0,
    height: 0
  }
}

export interface FileUploadRequest {

  id: string;

}
