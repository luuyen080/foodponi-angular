import {Address, INITIAL_ADDRESS} from "./address";

export interface User {

  id: string;

  avatar: string;

  email: string;

  username: string;

  role: string;

  status: boolean;

  address: Address;

}

export const INITIAL_USER: User = {
  id: '',
  avatar: '',
  email: '',
  username: '',
  role: '',
  status: false,
  address: INITIAL_ADDRESS
}

export interface UserIdRequest {

  id: string;

}

export interface UserAvatarRequest {

  avatar: string;

}

export interface UserRequestLogin {

  username: string | null;

  email: string | null;

  password: string;

}

export interface CurrentUser {
  id: string,
  sub: string,
  role: string,
  fullName: string,
  avatar: string,
  email: string,
  phoneNumber: string,
  username: string,
  accessToken: string
}

export interface IUserRemember {
  username: string;
  password: string;
  avatar: string;
}
