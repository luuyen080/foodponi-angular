import {INITIAL_USER, User} from "./user";
import {ProductDetail} from "./product";
import {Address, INITIAL_ADDRESS} from "./address";

export interface Order {
  id: string;
  totalAmount: number;
  user: User;
  shippingAddress: Address;
  orderItems: OrderItem[];
  note: string;
  payment: Payment;
  status: boolean;
  createdDate: string;
}

export interface OrderItem {
  id: string;
  productDetail: ProductDetail;
  quantity: number;
  price: number;
  note: string
}

export interface Payment {
  id: string;
  method: string;
  status: string;
  urlVNPAY: string;
}

export const INITIAL_PAYMENT: Payment = {
  id: '',
  method: '',
  status: '',
  urlVNPAY: '',
}

export const INITIAL_ORDER: Order = {
  id: '',
  totalAmount: 0,
  user: INITIAL_USER,
  shippingAddress: INITIAL_ADDRESS,
  orderItems: [],
  payment: INITIAL_PAYMENT,
  note: '',
  status: false,
  createdDate: ''
}
