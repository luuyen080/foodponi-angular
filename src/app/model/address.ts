export interface Address {

  id: string;

  fullName: string;

  phoneNumber: string;

  address: string;

  district: string;

  province: string;

  ward: string;

  street: string;

}

export const INITIAL_ADDRESS: Address = {
  id: '',
  fullName: '',
  phoneNumber: '',
  address: '',
  district: '',
  province: '',
  ward: '',
  street: '',
}
