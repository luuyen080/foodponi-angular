import {INITIAL_USER, User, UserIdRequest} from "./user";
import {Category, CategoryRequest} from "./category";

export interface Product {

  id: string;

  name: string;

  slug: string;

  shortDescription: string;

  thumbnail: string;

  status: boolean;

  user: User;

  productDetails: ProductDetail[];

  categories: Category[];

}

export const INITIAL_PRODUCT: Product = {
  id: '',
  name: '',
  slug: '',
  shortDescription: '',
  thumbnail: '',
  status: false,
  user: INITIAL_USER,
  productDetails: [],
  categories: []
}

export interface ProductRequest {

  id?: string;

  name: string;

  slug: string;

  shortDescription?: string;

  thumbnail?: string;

  status: boolean;

  user: UserIdRequest;

  productDetails: ProductDetailRequest[];

  categories: CategoryRequest[];

}

export interface ProductDetail {

  id: string;

  name: string;

  price: string;

  description: string;

  status: boolean;

  images: string[];

  product: Product;

}

export interface ProductDetailRequest {

  id?: string;

  name?: string;

  price: string;

  description?: string;

  status: boolean;

  images: string[];

}


