import {createReducer, on} from "@ngrx/store";
import {CurrentUser, UserAvatarRequest} from "../../model/user";
import {createSlice, PayloadAction} from "ngrx-slice";

export const INITIAL_CURRENT_USER = {
  id: '',
  sub: '',
  role: '',
  fullName: '',
  avatar: '',
  email: '',
  phoneNumber: '',
  username: '',
  accessToken: ''
}

export interface CurrentUserState {
  currentUser: CurrentUser;
}

export const initialState: CurrentUserState = {
  currentUser: INITIAL_CURRENT_USER
}

interface SetCurrentUserAction {
  type: string;
  currentUser: CurrentUser;
}

export const {
  actions: UserActions,
  selectors: UserSelectors,
  ...UserFeature
} = createSlice({
  name: 'user',
  initialState: initialState,
  reducers: {
    setCurrentUser: (state, currentUser: PayloadAction<CurrentUser>) => ({
      ...state,
      currentUser
    }),
    updateAvatar: (state, payload: PayloadAction<UserAvatarRequest>) => {
      return {
        ...state,
        currentUser: {...state.currentUser, avatar: payload.avatar}
      }
    }
  }
});
