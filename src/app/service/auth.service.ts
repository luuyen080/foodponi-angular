import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {UserRequestLogin} from "../model/user";
import {CookieService} from "ngx-cookie-service";
import {HOST} from "../utils/server";
import {Store} from "@ngrx/store";
import {UserSelectors} from "../store/user/user.reducer";
import {Observable} from "rxjs";
import {IToken} from "../model/auth";


@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private host = HOST + "/auth";

  constructor(
    private store: Store,
    private cookieService: CookieService,
    private http: HttpClient) {
  }

  login = (user: UserRequestLogin) => this.http.post(this.host + '/login', user);

  refreshToken = (refreshToken: string):Observable<IToken> => this.http.post<IToken>(this.host + '/refresh-token', {refreshToken: refreshToken});

  getToken = () => {
    let token = '';
    this.store.select(UserSelectors.selectCurrentUser).subscribe(currentUser => token = currentUser.accessToken);
    return token;
  }

  getIdCurrentUser = () => {
    let id = '';
    this.store.select(UserSelectors.selectCurrentUser).subscribe(currentUser => id = currentUser.id);
    return id;
  }
}
