import {Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {catchError, map, Observable, of} from "rxjs";
import {IProduct} from "../page/product/list/all-product.page";
import {HOST} from "../utils/server";
import {Store} from "@ngrx/store";
import {UserSelectors} from "../store/user/user.reducer";
import {INITIAL_PRODUCT, Product, ProductRequest} from "../model/product";
import {INITIAL_PAGE, Page} from "../model/common";
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private authService: AuthService,
    private http: HttpClient) {
  }

  getProducts(pageIndex: number, pageSize: number): Observable<Page<Product[]>> {

    let params = new HttpParams()
      .append('page', `${pageIndex}`)
      .append('pageSize', `${pageSize}`);
    return this.http.get<Page<Product[]>>(HOST + "/retailer/products", {
      params,
      headers: {'Authorization': 'Bearer ' + this.authService.getToken()}
    })
      .pipe(map(products => products ? products : INITIAL_PAGE));
  }

  getProduct(id: string): Observable<Product> {
    return this.http.get<Product>(HOST + "/products/" + id, {headers: {'Authorization': 'Bearer ' + this.authService.getToken()}});
  }

  createProduct(product: ProductRequest): Observable<Product> {
    return this.http.post<Product>(HOST+"/products", product, {headers: {'Authorization': 'Bearer ' + this.authService.getToken()}});
  }

  updateProduct(id: string, product: ProductRequest): Observable<Product> {
    return this.http.patch<Product>(HOST + "/products/" + id, product, {headers: {'Authorization': 'Bearer ' + this.authService.getToken()}});
  }

  deleteProduct(id: string): Observable<void> {
    return this.http.delete<void>(HOST + "/retailer/products/" + id, {headers: {'Authorization': 'Bearer ' + this.authService.getToken()}});
  }

  isExistedBySlug(slug: string): Observable<boolean> {
    let params = new HttpParams()
      .append('slug', slug);
    return this.http.get<boolean>(HOST + "/products/is-existed-by-slug",
      {
        params,
        headers: {'Authorization': 'Bearer ' + this.authService.getToken()}
      });
  }
}
