import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {catchError, map, Observable, of, switchMap, throwError} from "rxjs";
import {IUser} from "../page/user/list/all-user.page";
import {HOST} from "../utils/server";
import {INITIAL_PAGE, Page} from "../model/common";
import {User} from "../model/user";
import {AuthService} from "./auth.service";
import {INITIAL_ORDER, Order} from "../model/order";

@Injectable({
  providedIn: 'root',
})
export class OrderService {

  constructor(
    private authService: AuthService,
    private http: HttpClient) {
  }


  getOrders = (pageIndex: number, pageSize: number): Observable<Page<Order[]>> => {
    let params = new HttpParams()
      .append('page', `${pageIndex}`)
      .append('pageSize', `${pageSize}`);
    return this.http.get<Page<Order[]>>(HOST + '/retailer/orders', {
      params,
      headers: {'Authorization': 'Bearer ' + this.authService.getToken()}
    }).pipe(map(page => {
      return page ? page : INITIAL_PAGE;
    }));
  }

  getOrder = (id: string): Observable<Order> => {
    let params = new HttpParams()
      .append('isManagement', 'true');

    return this.http.get<Order>(HOST + '/retailer/orders/' + id, {
      params,
      headers: {'Authorization': 'Bearer ' + this.authService.getToken()}
    }).pipe(map(order => order ? order : INITIAL_ORDER));
  }

}
