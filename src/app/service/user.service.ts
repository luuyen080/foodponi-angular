import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {catchError, map, Observable, of, switchMap, throwError} from 'rxjs';
import {IUser} from '../page/user/list/all-user.page';
import {HOST} from '../utils/server';
import {INITIAL_PAGE, Page} from '../model/common';
import {User, UserAvatarRequest} from '../model/user';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {

  constructor(
    private authService: AuthService,
    private http: HttpClient) {
  }

  changePassword(oldPassword: String, newPassword: String): Observable<any> {
    return this.http.put(HOST + '/users/change-password',
      {oldPassword, newPassword},
      {
        headers: {'Authorization': 'Bearer ' + this.authService.getToken()}
      });
  }

  getUsers = (pageIndex: number, pageSize: number): Observable<Page<User[]>> => {
    let params = new HttpParams()
      .append('page', `${pageIndex}`)
      .append('pageSize', `${pageSize}`);
    return this.http.get<Page<User[]>>(HOST + '/admin/users', {
      params,
      headers: {'Authorization': 'Bearer ' + this.authService.getToken()}
    }).pipe(map(page => {
      return page ? page : INITIAL_PAGE;
    }));
  }

  getUser = (id: string): Observable<User> => this.http.get<User>(HOST + '/users/' + id);

  update = (id: string, field: object): Observable<User> => this.http.patch<User>(HOST + '/users/' + id, field);

  deleteUser = (id: string): Observable<void> => this.http.delete<void>(HOST + '/users/' + id);

  updateAvatar(userAvatar: UserAvatarRequest): Observable<User> {
    return this.http.put<User>(HOST + '/users/update-avatar', userAvatar, {
      headers: {'Authorization': 'Bearer ' + this.authService.getToken()}
    });
  }
}
