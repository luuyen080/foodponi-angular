import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {HOST} from "../utils/server";
import {Category} from "../model/category";
import {INITIAL_PAGE, Page} from "../model/common";

@Injectable({
  providedIn: 'root',
})
export class ProductCategoryService {
  private host = HOST + "/product-categories";

  constructor(private http: HttpClient) {
  }

  getCategoriesIsOnlyParent(): Observable<Page<Category[]>> {
    return this.http.get<Page<Category[]>>(this.host + "?onlyParent=true")
      .pipe(map(page => page ? page : INITIAL_PAGE));
  }

}
