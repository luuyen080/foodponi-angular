import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {HOST} from "../utils/server";
import {AuthService} from "./auth.service";
import {INITIAL_PAGE, Page} from "../model/common";
import {FileUpload} from "../model/file_upload";

@Injectable({
  providedIn: 'root',
})
export class FileUploadService {
  private host = HOST + "/file-uploads";

  constructor(
    private authService: AuthService,
    private http: HttpClient) {
  }

  uploadSingleFile(file: File): Observable<any> {
    const formData = new FormData();
    formData.append('multipartFile', file);
    return this.http.post(this.host, formData, {
      reportProgress: true,
      observe: 'events',
      headers: {
        'Authorization': 'Bearer' + this.authService.getToken()
      }
    });
  }

  getFileUploads(pageIndex: number, pageSize: number): Observable<Page<FileUpload[]>> {
    return this.http.get<Page<FileUpload[]>>(this.host, {
      headers: {
        'Authorization': 'Bearer ' + this.authService.getToken()
      }
    }).pipe(map(page => page ? page : INITIAL_PAGE));
  }

  deleteFileUpload(id: string): Observable<void> {
    return this.http.delete<void>(this.host + "/" + id);
  }

}
