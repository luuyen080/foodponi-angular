import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {catchError, Observable, switchMap, throwError} from "rxjs";
import {Injectable} from "@angular/core";
import {AuthService} from "../service/auth.service";
import {CookieService} from "ngx-cookie-service";
import {Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {INITIAL_CURRENT_USER, UserActions} from "../store/user/user.reducer";
import jwtDecode from "jwt-decode";
import {IToken} from "../model/auth";
import {HEADER_TOKEN, REFRESH_TOKEN} from "../utils/server";
import {CurrentUser} from "../model/user";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private store: Store,
    private router: Router,
    private cookieService: CookieService,
    private authService: AuthService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(catchError(error => {
        if (error.status === 401) {
          const refreshToken = this.cookieService.get(REFRESH_TOKEN);
          return this.authService.refreshToken(refreshToken).pipe(
            switchMap((res) => {
              const {accessToken, refreshToken} = res;
              this.store.dispatch(UserActions.setCurrentUser({...jwtDecode(accessToken) as CurrentUser, accessToken}));
              this.cookieService.set(REFRESH_TOKEN, refreshToken, {
                expires: new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30),
              });
              return next.handle(req.clone({headers: req.headers.set(HEADER_TOKEN, 'Bearer ' + accessToken)}));
            }),
            catchError(err => {
              // this.cookieService.delete(REFRESH_TOKEN);
              // this.store.dispatch(UserActions.setCurrentUser(INITIAL_CURRENT_USER));
              // this.router.navigate(['/login']);
              return throwError(err);
            })
          );
        }
        return throwError(error);
      }));
  }

}
