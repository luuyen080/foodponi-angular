export const REMEMBER_ME = 'rememberMe';
export const ACCESS_TOKEN = 'accessToken';
export const REFRESH_TOKEN = 'refreshToken';
export const HOST = 'http://localhost:8080/api/v1';
export const HEADER_TOKEN = 'Authorization';
